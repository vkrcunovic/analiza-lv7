﻿/*
 * Created by SharpDevelop.
 * User: Krcko
 * Date: 29.1.2019.
 * Time: 2:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace IksOks
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.PictureBox pb00;
		private System.Windows.Forms.PictureBox pb01;
		private System.Windows.Forms.PictureBox pb02;
		private System.Windows.Forms.PictureBox pb10;
		private System.Windows.Forms.PictureBox pb11;
		private System.Windows.Forms.PictureBox pb12;
		private System.Windows.Forms.PictureBox pb20;
		private System.Windows.Forms.PictureBox pb21;
		private System.Windows.Forms.PictureBox pb22;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button button2;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.pb00 = new System.Windows.Forms.PictureBox();
			this.pb01 = new System.Windows.Forms.PictureBox();
			this.pb02 = new System.Windows.Forms.PictureBox();
			this.pb10 = new System.Windows.Forms.PictureBox();
			this.pb11 = new System.Windows.Forms.PictureBox();
			this.pb12 = new System.Windows.Forms.PictureBox();
			this.pb20 = new System.Windows.Forms.PictureBox();
			this.pb21 = new System.Windows.Forms.PictureBox();
			this.pb22 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pb00)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb01)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb02)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pb22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pb00
			// 
			this.pb00.Location = new System.Drawing.Point(0, 0);
			this.pb00.Name = "pb00";
			this.pb00.Size = new System.Drawing.Size(50, 50);
			this.pb00.TabIndex = 1;
			this.pb00.TabStop = false;
			this.pb00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb00MouseUp);
			// 
			// pb01
			// 
			this.pb01.Location = new System.Drawing.Point(56, 0);
			this.pb01.Name = "pb01";
			this.pb01.Size = new System.Drawing.Size(50, 50);
			this.pb01.TabIndex = 2;
			this.pb01.TabStop = false;
			this.pb01.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb01MouseUp);
			// 
			// pb02
			// 
			this.pb02.Location = new System.Drawing.Point(112, 0);
			this.pb02.Name = "pb02";
			this.pb02.Size = new System.Drawing.Size(50, 50);
			this.pb02.TabIndex = 3;
			this.pb02.TabStop = false;
			this.pb02.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb02MouseUp);
			// 
			// pb10
			// 
			this.pb10.Location = new System.Drawing.Point(0, 56);
			this.pb10.Name = "pb10";
			this.pb10.Size = new System.Drawing.Size(50, 50);
			this.pb10.TabIndex = 4;
			this.pb10.TabStop = false;
			this.pb10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb10MouseUp);
			// 
			// pb11
			// 
			this.pb11.Location = new System.Drawing.Point(56, 56);
			this.pb11.Name = "pb11";
			this.pb11.Size = new System.Drawing.Size(50, 50);
			this.pb11.TabIndex = 5;
			this.pb11.TabStop = false;
			this.pb11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb11MouseUp);
			// 
			// pb12
			// 
			this.pb12.Location = new System.Drawing.Point(112, 56);
			this.pb12.Name = "pb12";
			this.pb12.Size = new System.Drawing.Size(50, 50);
			this.pb12.TabIndex = 6;
			this.pb12.TabStop = false;
			this.pb12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb12MouseUp);
			// 
			// pb20
			// 
			this.pb20.Location = new System.Drawing.Point(0, 112);
			this.pb20.Name = "pb20";
			this.pb20.Size = new System.Drawing.Size(50, 50);
			this.pb20.TabIndex = 7;
			this.pb20.TabStop = false;
			this.pb20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb20MouseUp);
			// 
			// pb21
			// 
			this.pb21.Location = new System.Drawing.Point(56, 112);
			this.pb21.Name = "pb21";
			this.pb21.Size = new System.Drawing.Size(50, 50);
			this.pb21.TabIndex = 8;
			this.pb21.TabStop = false;
			this.pb21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb21MouseUp);
			// 
			// pb22
			// 
			this.pb22.Location = new System.Drawing.Point(112, 112);
			this.pb22.Name = "pb22";
			this.pb22.Size = new System.Drawing.Size(50, 50);
			this.pb22.TabIndex = 9;
			this.pb22.TabStop = false;
			this.pb22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pb22MouseUp);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(162, 162);
			this.pictureBox1.TabIndex = 10;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox1Paint);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(168, 112);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(134, 50);
			this.button1.TabIndex = 11;
			this.button1.Text = "Play game!";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(168, 30);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(101, 20);
			this.textBox1.TabIndex = 12;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(170, 5);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(103, 22);
			this.label1.TabIndex = 13;
			this.label1.Text = "Player 1:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(168, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(103, 22);
			this.label2.TabIndex = 15;
			this.label2.Text = "Player 2:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(168, 81);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(101, 20);
			this.textBox2.TabIndex = 14;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(168, 27);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(103, 22);
			this.label3.TabIndex = 16;
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(166, 79);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(103, 22);
			this.label4.TabIndex = 17;
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.label5.Location = new System.Drawing.Point(279, 30);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(22, 20);
			this.label5.TabIndex = 18;
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.label6.Location = new System.Drawing.Point(279, 79);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(22, 22);
			this.label6.TabIndex = 19;
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(2, 165);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(30, 23);
			this.label7.TabIndex = 20;
			this.label7.Text = "Igra:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label8.Location = new System.Drawing.Point(29, 165);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(133, 22);
			this.label8.TabIndex = 21;
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(169, 165);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(132, 25);
			this.button2.TabIndex = 22;
			this.button2.Text = "Exit";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(307, 261);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.pb00);
			this.Controls.Add(this.pb22);
			this.Controls.Add(this.pb21);
			this.Controls.Add(this.pb20);
			this.Controls.Add(this.pb12);
			this.Controls.Add(this.pb11);
			this.Controls.Add(this.pb10);
			this.Controls.Add(this.pb02);
			this.Controls.Add(this.pb01);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Name = "MainForm";
			this.Text = "IksOks";
			((System.ComponentModel.ISupportInitialize)(this.pb00)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb01)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb02)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pb22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
