﻿/*
 * Created by SharpDevelop.
 * User: Krcko
 * Date: 29.1.2019.
 * Time: 2:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace IksOks
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		Graphics a00,a01,a02,
				b10,b11,b12,
				c20,c21,c22;				
		Pen blackPen = new Pen(Color.Black,2);
		XOgame game;
		int score1,score2;

		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			textBox1.Focus();
			a00=pb00.CreateGraphics();
			a01=pb01.CreateGraphics();
			a02=pb02.CreateGraphics();
			b10=pb10.CreateGraphics();
			b11=pb11.CreateGraphics();
			b12=pb12.CreateGraphics();
			c20=pb20.CreateGraphics();
			c21=pb21.CreateGraphics();
			c22=pb22.CreateGraphics();
			score1=0;
			score2=0;
			
				//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		class XOgame
		{
			public string player1{get;set;}
			public string player2{get;set;}
			public int onTurn;
			int[][] board;
			public XOgame(string p1, string p2)
			{
				player1=p1;
				player2=p2;
				onTurn=1;
				board=new int[3][];
				for (int i = 0; i < 3; i++) 
				{
					board[i]=new int[3];
				}
				for (int i = 0; i < 3; i++) 
				{
					for (int j = 0; j < 3; j++) 
					{
						board[i][j]=0;
					}
				}
			}
			public void playTurn(int i, int j)
			{
				if(onTurn==1)
				{
					board[i][j]=1;
					onTurn=2;
				}
				else
				{
					board[i][j]=2;
					onTurn=1;
				}
			}
			public bool legalMove(int i, int j)
			{
				return board[i][j]==0;
			}
			public bool victory()
			{
				int i00=board[0][0];
				int i01=board[0][1];
				int i02=board[0][2];
				int i10=board[1][0];
				int i11=board[1][1];
				int i12=board[1][2];
				int i20=board[2][0];
				int i21=board[2][1];
				int i22=board[2][2];
				return((i00==i01&&i00==i02&&i00!=0)||(i10==i11&&i10==i12&&i10!=0)||
				       (i20==i21&&i20==i22&&i20!=0)||(i00==i10&&i00==i20&&i00!=0)||
				       (i10==i11&&i10==i12&&i10!=0)||(i20==i21&&i20==i22&&i20!=0)||
				       (i00==i11&&i00==i22&&i00!=0)||(i02==i11&&i02==i20&&i02!=0));
			}
			public bool draw()
			{
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						if(board[i][j]==0) return false;
					}
				}
				return true;
			}
		}
		void putXO(Graphics g)
		{
			if(game.onTurn==1)
			{
				label8.Text=textBox1.Text;
				g.DrawEllipse(blackPen, 10,10,30,30);
			}
			else
			{
				label8.Text=textBox2.Text;
				g.DrawLine(blackPen,10,10,40,40);
				g.DrawLine(blackPen,40,10,10,40);
			}
		}
		
		void PictureBox1Paint(object sender, PaintEventArgs e)
		{			
			e.Graphics.DrawLine(blackPen, 0, 53, 165, 53);
			e.Graphics.DrawLine(blackPen, 0, 109, 165, 109);
			e.Graphics.DrawLine(blackPen, 53, 0, 53, 165);
			e.Graphics.DrawLine(blackPen, 109, 0, 109, 165);
		}
		
		
		void Button1Click(object sender, EventArgs e)
		{	game = new XOgame(textBox1.Text,textBox2.Text);
			if(!(textBox1.Text.ToString()=="") && !(textBox2.Text.ToString()=="")){
			textBox1.Visible=false;
			label3.Text=textBox1.Text;
			textBox2.Visible=false;
			label4.Text=textBox2.Text;			
			label8.Text=label3.Text;
			label5.Text=score1.ToString();
			label6.Text=score2.ToString();
			pb00.Visible=true;				
			pb01.Visible=true;
			pb02.Visible=true;				
			pb10.Visible=true;
			pb11.Visible=true;				
			pb12.Visible=true;
			pb20.Visible=true;				
			pb21.Visible=true;
			pb22.Visible=true;
			}
			else 
			{MessageBox.Show("Must enter both names!");
				textBox1.Focus();
			}
		}
		public void gameover()
		{
			if(game.victory())
			{
				if(game.onTurn==2)
				{	
					score1++;
					label5.Text=score1.ToString();
					MessageBox.Show("Winner is " + game.player1);
				}
				else
				{
					score2++;
					label6.Text=score2.ToString();
					MessageBox.Show("Winner is " + game.player2);
				}
			}
			else if(game.draw())
			{
				MessageBox.Show("It's a draw!");
			}
			if(game.draw() || game.victory())
			{
				pb00.Visible=false;				
				pb01.Visible=false;
				pb02.Visible=false;				
				pb10.Visible=false;
				pb11.Visible=false;				
				pb12.Visible=false;
				pb20.Visible=false;				
				pb21.Visible=false;
				pb22.Visible=false;
			}
		}
		void Button2Click(object sender, EventArgs e)
		{
			Application.Exit();
		}
		void Pb00MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(0,0))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(0,0);
				putXO(a00);
				gameover();
			}
		}
		void Pb01MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(0,1))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(0,1);
				putXO(a01);
				gameover();
			}
		}
		void Pb02MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(0,2))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(0,2);
				putXO(a02);
				gameover();
			}
		}
		void Pb10MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(1,0))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(1,0);
				putXO(b10);
				gameover();
			}
		}
		void Pb11MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(1,1))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(1,1);
				putXO(b11);
				gameover();
			}
		}
		void Pb12MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(1,2))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(1,2);
				putXO(b12);
				gameover();
			}
		}
		void Pb20MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(2,0))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(2,0);
				putXO(c20);
				gameover();
			}
		}
		void Pb21MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(2,1))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(2,1);
				putXO(c21);
				gameover();
			}
		}
		void Pb22MouseUp(object sender, MouseEventArgs e)
		{
			if(!game.legalMove(2,2))
				System.Media.SystemSounds.Exclamation.Play();
			else
			{
				game.playTurn(2,2);
				putXO(c22);
				gameover();
			}
		}
	}
}
